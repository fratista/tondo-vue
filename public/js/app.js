window.onload = function() {
    var vm = new Vue({
        el: '#app',
        data: {
            measurements: {
                length: 100,
                width: 50,
                height: 50
            },
            wall: {
                thickness: 16,
                doubleFrame: 16
            },
            foot: {
                footHeight: 1.6,
                skirt: {
                    front: 0,
                    left: 0,
                    back: 0,
                    right: 0
                }
            },
            surface: {
                material: 'Standardfarbe',
                specification: {
                    standard: 'Anthrazit',
                    ral: 'ccc58f',
                    ncs: '',
                    buntsteinputz: '',
                    reibputz: ''
                }
            },
            extras: {
                waterdrain: 'entleerung',
                profile: {
                    front: false,
                    left: false,
                    back: false,
                    right: false
                },
                styropor: false,
                leca: false,
                vlies: false,
                message: ''
            },
            statics: {
                weight: 39,
                weightEarth: 294,
                weightEarthWet: 440,
                volume: 212,
                surfaceArea: 3.9,
                bendingWall: 2,
                bendingFloor: 2,
                middlePiece: false,
                doubleFrame: 1.6,
                wallCorrection: 16,
                price: 401.99,
                error: ''
            }
        },
        methods: {
            calculateTrough: function() {
                this.statics.weight = data['weight_empty']
                this.statics.weightEarth = data['weight_earth']
                this.statics.weightEarthWet = data['weight_earth_wet']
                this.statics.volume = data['earth_volume']
                this.statics.surfaceArea = data['surface']
                this.statics.bendingWall = data['bending_wall']
                this.statics.bendingFloor = data['bending_floor']
                this.statics.middlePiece = data['middle_piece']
                this.statics.doubleFrame = data['double_frame']
                this.statics.wallCorrection = data['wall_thickness']
                this.statics.price = data['trough_price']
                this.statics.error = data['error']
            },
            zoomIn: function() {

            },
            zoomOut: function() {

            },
            cameraFront: function() {

            },
            cameraRear: function() {

            },
            cameraBottom: function() {

            },
            cameraTop: function() {

            },
            initializeConfigurator: function() {
                $('#request-form input').off('keydown').on('keydown', function() {
                    requestReady();
                });

                $('#request-modal .request-offer').off('click').on('click', function() {
                    sendOffer();
                });
            },
            /**
             * Validate values before calculation.
             *
             *
             */
            validateMeasures: function(input, minValue, maxValue) {
                if (Number($("input[name=" + input + "]").val() >= minValue) && Number($("input[name=" + input + "]").val() <= maxValue)) {
                    input = Number($("input[name=" + input + "]").val());
                } else if (Number($("input[name=" + input + "]").val() <= minValue)) {
                    $("input[name=" + input + "]").val(minValue);
                    input = Number($("input[name=" + input + "]").val());
                } else if (Number($("input[name=" + input + "]").val() >= maxValue)) {
                    $("input[name=" + input + "]").val(maxValue);
                    input = Number($("input[name=" + input + "]").val());
                }

                return input;
            },
            /**
             *
             *
             * Hide color pickers for other color options.
             */
            colorPicker: function() {
                $(".color-specification-input").not(".color-picker-standard").prop("disabled", true).css('display', 'none');

                $(".color-option").click(function() {
                    var colorOption = $(this).attr('class').substr(19);

                    $(".color-picker-" + colorOption).prop("disabled", false).css('display', 'block');

                    $(".color-specification-input").not(".color-picker-" + colorOption).prop("disabled", true).css('display', 'none');
                });
            },
            /**
             * Switch wall strength and call method to switch doppio strength as well.
             *
             *
             */
            switchWall: function() {
                var opposite = (this.wall.thickness == 16) ? 24 : 16;
                var doppio = (this.wall.thickness == 16) ? 32 : 40;
                var doppioOpposite = (this.wall.thickness == 16) ? 40 : 32;

                $("label[for=doppio" + this.wall.thickness + "]").html("Standard");
                $("label[for=doppio" + opposite + "]").html(opposite + " mm");
                $("#doppio" + opposite + ", #doppio" + opposite + " + label, #doppio" + doppioOpposite + ", #doppio" + doppioOpposite + " + label").css('display', 'none').prop('disabled', false);
                $("#doppio" + this.wall.thickness + ", #doppio" + this.wall.thickness + " + label, #doppio" + doppio + ", #doppio" + doppio + " + label").css('display', 'block').prop('disabled', false);

                $('#doppio' + opposite).prop('checked', false);
                $('#doppio' + this.wall.thickness).prop('checked', true);
            },
            /**
             * Switch panels for configuration options.
             *
             *
             */
            switchPanels: function(panel) {
                $(".pager-button").removeClass('active');
                $(event.target.parentElement).addClass('active');

                $(".instructions").html(event.target.parentElement.title);
                $(".option-panel").removeClass('active');
                $(".option-panel." + panel + "-panel").addClass('active');
            },
            /**
             * Initialize rendering with default values.
             *
             *
             */
            renderTrough: function() {
                var Trough = function(troughValues) {
                    this.length = troughValues[0];
                    this.width = troughValues[1];
                    this.height = troughValues[2];
                    this.wallThickness = troughValues[3];
                    this.footHeight = troughValues[4];
                    this.skirtFront = troughValues[5];
                    this.skirtLeft = troughValues[6];
                    this.skirtBack = troughValues[7];
                    this.skirtRight = troughValues[8];
                    this.colorOption = troughValues[9];
                    this.colorSpecification = troughValues[10];
                    this.doubleFrame = troughValues[11];
                    this.profileFront = troughValues[12];
                    this.profileLeft = troughValues[13];
                    this.profileBack = troughValues[14];
                    this.profileRight = troughValues[15];
                    this.styropor = troughValues[16];
                    this.vlies = troughValues[17];
                    this.leca = troughValues[18];
                    this.middlePiece = troughValues[19];
                    this.rotationX = troughValues[20];
                    this.rotationY = troughValues[21];
                    this.zoomOffset = troughValues[22];
                }

                window.trough = new Trough([100, 50, 50, 1.6, 1.6, 0, 0, 0, 0, 'Standardfarbe', '363d43', 1.6, 0, 0, 0, 0, false, false, false, false, 0.08, 0.95, 0]);

                initializeRendering(trough);
            },
            /**
             * Calculation and re-rendering.
             *
             *
             */
            calculateTrough: function() {
                $(".modal, .modal-container.information").addClass('in');
                $(".modal-container.information h2").html('Der Trog wird berechnet');

                var form = $("#configuration-form").serializeArray();
                var fields = {};

                for (var i = 0; i < form.length; i++) {
                    var name = form[i].name;
                    var value = form[i].value;

                    if (name) {
                        fields[name] = value;
                    }
                }

                $.ajax({
                    url: '/configurator/calculate',
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: fields,
                    dataType: 'json',
                    success: function(data) {
                        this.statics.volume = data['earth_volume'];
                        this.statics.weight = data['weight_empty'];
                        this.statics.weightEarth = data['weight_earth'];
                        this.statics.bendingWall = data['bending_wall'];
                        this.statics.bendingFloor = data['bending_floor'];
                        this.statics.surface = data['surface'];
                        trough.middlePiece = data['middle_piece'];
                        this.statics.price = data['trough_price'];
                        this.statics.error = data['error'];
                        this.statics.wallCorrection = data['wall_thickness'];
                        this.statics.doubleFrame = data['double_frame'];

                        if (this.statics.wallCorrection == 0.024) {
                            $('input:radio[name="wall-thickness"][value=24]').prop('checked', true);
                            $('input:radio[name="wall-thickness"][value=16]').prop('checked', false);
                        } else {
                            $('input:radio[name="wall-thickness"][value=16]').prop('checked', true);
                            $('input:radio[name="wall-thickness"][value=24]').prop('checked', false);
                        }

                        //var thickness = wallCorrection * 100;
                        var thickness = Number($('input[name=wall-thickness]:checked').val() / 10);;

                        $(".price-box .price, #mobile-price-box .price").html(price.toFixed(2) + " €");
                        $(".price-box .price-vat .price-value, #mobile-price-box .price-vat .price-value").html((price * 1.2).toFixed(2) + " €");
                        $(".error-wrapper h5").text(error);

                        trough.length = validateMeasures("length", 16, 200);
                        trough.width = validateMeasures("width", 20, 120);
                        trough.height = validateMeasures("height", 10, 100);
                        trough.wallThickness = thickness;

                        trough.footHeight = validateMeasures("foot-height", 1.6, 10);
                        trough.skirtFront = validateMeasures("skirt-front", 0, 30);
                        trough.skirtLeft = validateMeasures("skirt-left", 0, 30);
                        trough.skirtBack = validateMeasures("skirt-back", 0, 30);
                        trough.skirtRight = validateMeasures("skirt-right", 0, 30);

                        trough.colorOption = $("input[name=color-option]:checked").val();

                        switch (trough.colorOption) {
                            case 'Standardfarbe':
                                trough.colorSpecification = $("input[name=standard-ral]:checked").val();
                                break;
                            case 'RAL-Farbe':
                                trough.colorSpecification = $(".color-specification-input.color-picker-ral option:selected").val();
                                break;
                            case 'NCS-Farbe':
                                trough.colorSpecification = $(".color-specification-input.color-picker-ncs").val();
                                break;
                            case 'Buntsteinputz':
                                trough.colorSpecification = $(".color-specification-input.color-picker-buntsteinputz").val();
                                break;
                            case 'Reibputz':
                                trough.colorSpecification = $(".color-specification-input.color-picker-reibputz").val();
                                break;
                            default:
                                trough.colorSpecification = "363d43";
                        }

                        trough.doubleFrame = $("input[name=double-frame]:checked").val() / 10;
                        trough.profileFront = ($("input[name=profile-front]").prop('checked') == true) ? true : false;
                        trough.profileLeft = ($("input[name=profile-left]").prop('checked') == true) ? true : false;
                        trough.profileBack = ($("input[name=profile-back]").prop('checked') == true) ? true : false;
                        trough.profileRight = ($("input[name=profile-right]").prop('checked') == true) ? true : false;

                        trough.styropor = ($("input[name=styropor]").prop('checked') == true) ? true : false;
                        trough.vlies = ($("input[name=vlies]").prop('checked') == true) ? true : false;
                        trough.leca = ($("input[name=leca]").prop('checked') == true) ? true : false;

                        scene = null;
                        $('canvas').remove();
                        initializeRendering(trough);

                        storeStats(fields, data, thickness, doubleFrame, wallCorrection);
                    },
                    error: function() {

                    }
                });
            },
            /**
             * Save the values in the session storage for later retrieval
             * by the send offer function.
             *
             *
             */
            storeStats: function(fields, data, thickness, doubleFrame, wallCorrection) {
                if (typeof(Storage) !== "undefined") {
                    sessionStorage.setItem('fields', JSON.stringify(fields));
                    sessionStorage.setItem('volume', data['volume']);
                    sessionStorage.setItem('weightEmpty', data['weight_empty']);
                    sessionStorage.setItem('weightEarth', data['weight_earth']);
                    sessionStorage.setItem('bendingWall', data['bending_wall']);
                    sessionStorage.setItem('bendingFloor', data['bending_floor']);
                    sessionStorage.setItem('surface', data['surface']);
                    sessionStorage.setItem('middlePiece', data['middle_piece']);
                    sessionStorage.setItem('price', data['trough_price']);
                    sessionStorage.setItem('thickness', thickness);
                    sessionStorage.setItem('doubleFrame', doubleFrame);
                    sessionStorage.setItem('newWallThickness', wallCorrection);
                }

                return;
            },
            /**
             * Save the values in the session storage for later retrieval
             * by the send offer function.
             *
             *
             */
            fetchStats: function() {
                if (typeof(Storage) !== "undefined") {
                    /**
                     * If the user has not interacted with the form yet, a default calculation will be started.
                     */
                    if (sessionStorage.getItem('price') === null) {
                        calculateTrough();
                    }

                    var stats = {
                        'fields': JSON.parse(sessionStorage.getItem('fields')),
                        'volume': sessionStorage.getItem('volume'),
                        'weightEmpty': sessionStorage.getItem('weightEmpty'),
                        'weightEarth': sessionStorage.getItem('weightEarth'),
                        'bendingWall': sessionStorage.getItem('bendingWall'),
                        'bendingFloor': sessionStorage.getItem('bendingFloor'),
                        'surface': sessionStorage.getItem('surface'),
                        'middlePiece': sessionStorage.getItem('middlePiece'),
                        'price': sessionStorage.getItem('price'),
                        'thickness': sessionStorage.getItem('thickness'),
                        'doubleFrame': sessionStorage.getItem('doubleFrame')
                    };
                }

                return stats;
            },
            /**
             * Check if the user has filled out all fields of the contact form.
             *
             *
             */
            requestReady: function() {
                $("#request-form input").each(function() {
                    var element = $(this);

                    if (element.val() == "") {
                        $('.btn.request-offer').prop('disabled', true);

                        return;
                    }

                    $('.btn.request-offer').prop('disabled', false);
                });
            },
            /**
             * Send the offer for the configured product via e-mail.
             *
             *
             */
            sendOffer: function() {
                var contact = $('#request-form').serializeArray();
                var stats = fetchStats();

                $.ajax({
                    url: '/configurator/send-offer',
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        'contact': contact,
                        'stats': stats
                    },
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        setTimeout(function() {
                            $('body').removeClass('modal-open');
                            $('.modal').removeClass('show').css('display', 'none');
                            $('.modal-backdrop.show').remove();
                        }, 3000);
                    },
                    error: function() {
                        console.log('Error');
                    }
                });
            },
            /**
             * Switching camera angles and zoom for scene.
             *
             *
             */
            cameraOptions: function() {
                $(".modal, .modal-container.information").addClass('in');
                $(".modal-container.information h2").html('Die Perspektive wird geändert');

                var cameraOption = $(this).attr('class').split(' ')[1];

                switch (cameraOption) {
                    case "zoom-in":
                        trough.zoomOffset -= 10;
                        if (trough.zoomOffset < -10) trough.zoomOffset = -10;
                        break;
                    case "zoom-out":
                        trough.zoomOffset += 10;
                        if (trough.zoomOffset > 60) trough.zoomOffset = 60;
                        break;
                    case "camera-front":
                        trough.rotationY += 0.5;
                        break;
                    case "camera-rear":
                        trough.rotationY -= 0.5;
                        break;
                    case "camera-bottom":
                        trough.rotationX += 0.3;
                        break;
                    case "camera-top":
                        trough.rotationX -= 0.3;
                        break;
                }

                scene = null;
                $('canvas').remove();
                initializeRendering(trough);
                console.clear();
            }
        },
        computed: {
            /*
            weight: function() {
                return 39
            },
            weightEarth: function() {
                return 294
            },
            weightEarthWet: function() {
                return 440
            },
            volume: function() {
                return 212
            },
            surfaceArea: function() {
                return 3.9
            },
            bendingWall: function() {
                return 2
            }*/
        },
        created: function() {
            /**
             * Hide the initial doppio values.
             */
            $("#doppio24, label[for=doppio24], #doppio40, label[for=doppio40]").css('display', 'none').prop('disabled', true);

            this.colorPicker();

            /**
             * Initial rendering and calculation of the trough.
             */
            this.renderTrough();
            this.calculateTrough();

            $('.btn.request-offer').prop('disabled', true);

            axios
                .get('https://api.coindesk.com/v1/bpi/currentprice.json')
                .then(response => (console.log(response)));
        }
    })
}