<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Mail;

use App\Configurator;
use App\Offer;
use App\Mail\SendOffer;

class ConfiguratorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Calculate the trough.
     *
     * @return response
     */
    public function calculate(Request $request)
    {
        $configurator = new Configurator($request->all(), false);
        $configurator->fetchPrices();

        $bending = $this->recalculateDependencies($configurator, $request->input('wall-thickness') / 1000, false, '');

        /**
         * Reconfigure certain options based on the user input to provide optimum static values.
         * After reconfiguration, the new settings are returned to the front-end.
         */
        if ($configurator->measurements['length'] > 1.4 || $configurator->width > 0.5 || $configurator->measurements['height'] > 0.5) {
            $bending = $this->recalculateDependencies($configurator, 0.024, false, '');

            if ($bending > 0.001) {
                $bending = $this->recalculateDependencies($configurator, 0.024, true, 'Aus statischen Gründen wurde die Wandstärke erhöht und ein Mittelsteg eingefügt');
            } else {
                $bending = $this->recalculateDependencies($configurator, 0.024, false, 'Aus statischen Gründen wurde die Wandstärke erhöht');
            }
        } else {
            if ($bending > 0.002 && ($configurator->measurements['length'] != $configurator->measurements['width'])) {
                $bending = $this->recalculateDependencies($configurator, 0.016, true, 'Aus statischen Gründen wurde ein Mittelsteg eingefügt');
            } elseif ($bending > 0.002 && ($configurator->measurements['length'] == $configurator->measurements['width'])) {
                $bending = $this->recalculateDependencies($configurator, 0.016, false, '');
            } else {
                $bending = $this->recalculateDependencies($configurator, $request->input('wall-thickness') / 1000, false, '');
            }
        }

        $trough_data = [
            'volume' => $configurator->volume,
            'weight_empty' => $configurator->weight_empty,
            'weight_earth' => $configurator->weight_earth,
            'bending_wall' => $configurator->bending_wall,
            'bending_floor' => $configurator->bending_floor,
            'surface' => $configurator->surface,
            'middle_piece' => $configurator->middle_piece,
            'trough_price' => $configurator->trough_price,
            'error' => $configurator->error,
            'wall_thickness' => $configurator->wall_options['wall_thickness'],
            'double_frame' => $configurator->wall_options['double_frame']
        ];

        return Response::json($trough_data);
    }

    /**
    * Recalculate the dependencies of the calculation and pass
    * the configurator instance as reference to keep the state.
    *
    * @return Integer
    */
    public function recalculateDependencies(&$configurator, $thickness, $statics, $error)
    {
        $configurator->clearTrough();

        $configurator->wall_options['wall_thickness'] = $thickness;
        $configurator->floor_thickness = $thickness;

        $configurator->computeSurface();
        $configurator->computeDoubleBorder();
        $configurator->computeFeet();
        $configurator->computeColor();
        $configurator->computeSkirt();
        $configurator->computeMiddlePiece();
        $configurator->computeProfile();
        $configurator->computeWater();
        $configurator->computeAccessoires();
        $configurator->computeWeight();
        $configurator->computeStatics($statics);
        $configurator->computeMiddlePiece();
        $configurator->error = $error;
        $bending = $configurator->finishTrough();

        return $bending;
    }

    /**
     * Request an offer
     *
     * @return response
     */
    public function sendOffer(Offer $offer, Request $request)
    {
        /**
         * Assign product data to offer instance.
         */
        $offer->fields = $request->input('stats.fields');
        $offer->stats = $request->input('stats');

        $contact = [
            'email' => $request->input('contact')[0]['value'],
            'firstname' => $request->input('contact')[1]['value'],
            'lastname' => $request->input('contact')[2]['value'],
        ];

        Mail::to($contact['email'])->send(new SendOffer($offer, $contact));

        if (count(Mail::failures()) > 0) {
            $failures = [];

            foreach (Mail::failures() as $failure) {
                $failures[] = $failure;
            }

            return Response::json($failures);
        }

        return Response::json(1);
    }

    /**
     * Preview an offer
     *
     * @return SendOffer
     */
    public function previewOffer(Offer $offer, Request $request)
    {
        /**
         * Assign product data to offer instance.
         */
        $offer->product = $request->product;

        $contact = [
            'email' => 'fratista20@gmail.com',
            'firstname' => 'Franjo',
            'lastname' => 'Antunovic',
        ];

        Mail::to('fratista20@gmail.com')->send(new SendOffer($offer, $contact));

        return new SendOffer($offer, $contact);
    }
}
