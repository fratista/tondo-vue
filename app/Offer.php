<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    public $number;
    public $created;
    public $date;
    public $sum;

    public $product;

    /**
     * Create a new offer instance.
     *
     * @return void
     */
    public function __construct()
    {
        $days = [
            'Montag',
            'Dienstag',
            'Mittwoch',
            'Donnerstag',
            'Freitag',
            'Samstag',
            'Sonntag'
        ];

        $months = [
            1 => 'Januar',
            2 => 'Februar',
            3 => 'März',
            4 => 'April',
            5 => 'Mai',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'August',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Dezember'
        ];

        $day = $days[date('w')];
        $month = $months[date('n')];

        $this->date = $day . date(', d.') .$month . date(' Y');
    }
}
