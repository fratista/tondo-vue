<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Exception;

class Configurator extends Model
{
    /**
     * User input from the configuration form stored in associative
     * arrays grouped by the sections in the configurator.
     *
     * @return void
     */
    public $measurements = [
        'length',
        'width',
        'height'
    ];

    public $wall_options = [
        'wall_thickness',
        'double_frame'
    ];

    public $foot_options = [
        'foot_height',
        'skirt_front',
        'skirt_left',
        'skirt_back',
        'skirt_right'
    ];

    public $surface_options = [
        'color_option',
        'color_specification'
    ];

    public $water_option;

    public $profile_options = [
        'profile_front',
        'profile_left',
        'profile_back',
        'profile_right',
    ];

    public $additional_options = [
        'vlies',
        'leca',
        'styropor'
    ];

    public $material;
    public $floor_clearance;
    public $floor_thickness;
    public $notch_depth;

    public $middle_piece;

    private $ny16;
    private $ny18;
    private $ny20;
    private $ny24;
    private $ny28;
    private $glueing;
    private $inertol;
    private $ikosit;
    private $silikat;
    private $marmolit;
    private $working_hour;
    private $vlies_price;
    private $leca_price;
    private $styropor_price;
    private $offcut;
    private $hydro_density;
    private $skirt_factor;
    private $country_house_price;
    private $leca_filling;
    private $leca_height;
    private $et15;
    private $et20;

    private $inner_length;
    private $inner_width;
    private $inner_height;
    private $glue_notch;
    private $wall_area;
    private $floor_area;
    private $total_area;
    public $surface;
    public $weight_empty;
    public $weight_earth;
    public $volume;
    public $trough_price;
    public $bending_wall;
    public $bending_floor;
    public $error;

    /**
     * Intitialize the object and assign the base values for the trough.
     *
     * @return void
     */
    public function __construct($trough_parameters, $middlePiece)
    {
        if ($trough_parameters['length'] > 0) {
            if ($trough_parameters['length'] <= 200) {
                $this->measurements['length'] = $trough_parameters['length'] / 100;
            } else {
                $this->measurements['length'] = 2;
            }
        } else {
            $this->measurements['length'] = $trough_parameters['length'] / -100;
        }

        if ($trough_parameters['width'] > 0) {
            if ($trough_parameters['width'] <= 120) {
                $this->measurements['width'] = $trough_parameters['width'] / 100;
            } else {
                $this->measurements['width'] = 2;
            }
        } else {
            $this->measurements['width'] = $trough_parameters['width'] / -100;
        }

        if ($trough_parameters['height'] > 0) {
            if ($trough_parameters['height'] <= 120) {
                $this->measurements['height'] = $trough_parameters['height'] / 100;
            } else {
                $this->measurements['height'] = 2;
            }
        } else {
            $this->measurements['height'] = $trough_parameters['height'] / -100;
        }

        $this->wall_options['wall_thickness'] = $trough_parameters['wall-thickness'] / 1000;
        $this->wall_options['double_frame'] = $trough_parameters['double-frame'];

        $this->foot_options['foot_height'] = ($trough_parameters['foot-height'] > 0) ? $trough_parameters['foot-height'] / 1000 : $trough_parameters['foot-height'] / -1000;
        $this->foot_options['skirt_front'] = ($trough_parameters['skirt-front'] > 0) ? $trough_parameters['skirt-front'] / 100 : $trough_parameters['skirt-front'] / -100;
        $this->foot_options['skirt_left'] = ($trough_parameters['skirt-left'] > 0) ? $trough_parameters['skirt-left'] / 100 : $trough_parameters['skirt-left'] / -100;
        $this->foot_options['skirt_back'] = ($trough_parameters['skirt-back'] > 0) ? $trough_parameters['skirt-back'] / 100 : $trough_parameters['skirt-back'] / -100;
        $this->foot_options['skirt_right'] = ($trough_parameters['skirt-right'] > 0) ? $trough_parameters['skirt-right'] / 100 : $trough_parameters['skirt-right'] / -100;

        $this->surface_options['color_option'] = $trough_parameters['color-option'];

        switch ($this->surface_options['color_option']) {
          case 'Standardfarbe':
                $this->surface_options['color_specification'] = isset($trough_parameters['standard-ral']) ? $trough_parameters['standard-ral'] : "Anthrazitgrau";
                break;
          case 'RAL-Farbe':
                $this->surface_options['color_specification'] = isset($trough_parameters['color-picker-ral']) ? $trough_parameters['color-picker-ral'] : "1000 - Grünbeige";
                break;
          case 'NCS-Farbe':
                $this->surface_options['color_specification'] = $trough_parameters['color-picker-ncs'];
                break;
          case 'Buntsteinputz':
                $this->surface_options['color_specification'] = $trough_parameters['color-picker-buntsteinputz'];
                break;
          case 'Reibputz':
                $this->surface_options['color_specification'] = $trough_parameters['color-picker-reibputz'];
                break;
          default:
                $this->surface_options['color_specification'] = 'Anthrazitgrau';
        }

        $this->water_option = $trough_parameters['water-option'];
        $this->profile_options['profile_front'] = (isset($trough_parameters['profile-front'])) ? true : false;
        $this->profile_options['profile_left'] = (isset($trough_parameters['profile-left'])) ? true : false;
        $this->profile_options['profile_back'] = (isset($trough_parameters['profile-back'])) ? true : false;
        $this->profile_options['profile_right'] = (isset($trough_parameters['profile-right'])) ? true : false;

        $this->additional_options['vlies'] = (isset($trough_parameters['vlies'])) ? true : false;
        $this->additional_options['leca'] = (isset($trough_parameters['leca'])) ? true : false;
        $this->additional_options['styropor'] = (isset($trough_parameters['styropor'])) ? true : false;

        $this->material = "Betonyp";
        $this->floor_thickness = $this->wall_options['wall_thickness'];
        $this->floor_clearance = 16 / 1000;
        $this->notch_depth = 7 / 1000;
        $this->middle_piece = $middlePiece;
    }

    /**
     * Fetch material prices from the DB.
     *
     * @return void
     */
    public function fetchPrices()
    {
        $material_prices = [];
        $material_ids = [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 16, 17, 18, 28, 29, 35, 36, 47, 48, 49, 50];

        $materials = DB::table('prices')->select('price')->whereIn('id', $material_ids)->get();

        foreach ($materials as $material) {
            $material_prices[] = $material->price;
        }

        $this->ny16 = $material_prices[0];
        $this->ny18 = $material_prices[1];
        $this->ny20 = $material_prices[2];
        $this->ny24 = $material_prices[3];
        $this->ny28 = $material_prices[4];
        $this->glueing = $material_prices[5];
        $this->inertol = $material_prices[6];
        $this->ikosit = $material_prices[7];
        $this->silikat = $material_prices[8];
        $this->marmolit = $material_prices[9];
        $this->working_hour = $material_prices[10];
        $this->vlies_price = $material_prices[11];
        $this->leca_price = $material_prices[12];
        $this->styropor_price = $material_prices[13];
        $this->offcut = $material_prices[14];
        $this->hydro_density = $material_prices[15];
        $this->skirt_factor = $material_prices[16];
        $this->country_house_price = $material_prices[17];
        $this->leca_filling = $material_prices[18];
        $this->leca_height = $material_prices[19];
        $this->et15 = $material_prices[20];
        $this->et20 = $material_prices[21];
    }

    /**
     * Compute the surface based on the user input.
     *
     * @return void
     */
    public function computeSurface()
    {
        $this->inner_length = ($this->measurements['length'] - 2 * $this->wall_options['wall_thickness']);
        $this->inner_width = ($this->measurements['width'] - 2 * $this->wall_options['wall_thickness']);
        $this->inner_height = ($this->measurements['height'] - $this->floor_thickness - $this->floor_clearance);
        $this->glue_notch = (2 * $this->measurements['length'] + 2 * $this->measurements['width'] + 4 * $this->measurements['height']);
        $this->wall_area = (2 * $this->measurements['height'] * ($this->measurements['length'] + $this->measurements['width'] - 2 * $this->wall_options['wall_thickness'] + 2 * $this->notch_depth));
        $this->floor_area = ($this->measurements['length'] - 2 * $this->wall_options['wall_thickness'] + 2 * $this->notch_depth) * ($this->measurements['width'] - 2 * $this->wall_options['wall_thickness'] + 2 * $this->notch_depth);
        //$inner_area = (2 * ($this->inner_length + $this->inner_width) * $this->inner_height);
        $this->total_area = 2 * ((2 * ($this->measurements['length'] + $this->measurements['width']) * $this->measurements['height']) + $this->measurements['length'] * $this->measurements['width']);

        $this->surface = (($this->measurements['length'] * $this->measurements['height'] * 4) + ($this->measurements['width'] * $this->measurements['height'] * 4) + ($this->measurements['length'] * $this->measurements['width']));
    }

    /**
     * Compute the optional double border on the inside of the trough.
     *
     * @return void
     */
    public function computeDoubleBorder()
    {
        switch ($this->wall_options['double_frame']) {
            case 32:
            case 40:
            case 48:
                $this->total_area += (($this->inner_length * 0.1 + $this->inner_width * 0.1) * 2);
                break;
        }
    }

    /**
     * Compute the various feet options and add costs for glueing.
     *
     * @return void
     */
    public function computeFeet()
    {
        $foot_height_relevant = $this->foot_options['foot_height'] - 1.6 / 1000;
        $fussAnzahl = round(($this->measurements['length'] - 0.2) / 0.4, 0) + 1;
        $platten_costs = ((((($this->measurements['width'] - 0.1) * 2) + 0.2) * $fussAnzahl) * 3) * $foot_height_relevant * $this->ny24;

        $foot_glueing = $foot_height_relevant * 4 * $fussAnzahl * $this->glueing;
        $this->trough_price += $foot_glueing + $platten_costs;
    }

    /**
     * Compute the optional skirt at the bottom of each side.
     *
     * @return void
     */
    public function computeSkirt()
    {
        $schuerze_area = $this->measurements['length'] * ($this->foot_options['skirt_front'] + $this->foot_options['skirt_left'] + $this->foot_options['skirt_back'] + $this->foot_options['skirt_right']);
        $schuerze_extracharge = ($this->wall_options['wall_thickness'] == 0.016) ? $schuerze_area * $this->ny16 * $this->skirt_factor : $schuerze_area * $this->ny24 * $this->skirt_factor;

        $this->surface += $schuerze_area;
        $this->trough_price += $schuerze_extracharge;
    }

    /**
     * Compute the middle piece which can be optional. Depending on the values from the calculation, this can be added automatically.
     *
     * @return void
     */
    public function computeMiddlePiece()
    {
        $ny_type = ($this->wall_options['wall_thickness'] * 1000 == 16) ? $this->ny16 : $this->ny24;
        $et_type = ($this->wall_options['wall_thickness'] * 1000 == 16) ? $this->et15 : $this->et20;

        if ($this->middle_piece == false) {
            $middle_piece_price = 0;
        } else {
            if ($this->material = "Betonyp") {
                $middle_piece_price = ((($this->measurements['width'] - ($this->wall_options['wall_thickness'] * 2)) * ($this->measurements['height'] - $this->floor_thickness - $this->floor_clearance - 0.15)) *  $ny_type) + ($this->glueing * (($this->measurements['height'] - $this->floor_thickness - $this->floor_clearance - 0.15) * 2));
            } else {
                $middle_piece_price = ((($this->measurements['width'] - ($this->wall_options['wall_thickness'] * 2)) * ($this->measurements['height'] - $this->floor_thickness - $this->floor_clearance - 0.15)) *  $et_type) + ($this->glueing * (($this->measurements['height'] - $this->floor_thickness - $this->floor_clearance - 0.15) * 2));
            }
        }

        $this->trough_price += $middle_piece_price;
    }

    /**
     * Compute the color options.
     *
     * @return void
     */
    public function computeColor()
    {
        $ny_type = ($this->floor_thickness * 1000 == 16) ? $this->ny16 : $this->ny24;
        $et_type = ($this->floor_thickness * 1000 == 16) ? $this->et15 : $this->et20;

        if ($this->material = 'Betonyp') {
            $roh = $this->wall_area * $ny_type + $this->floor_area * $ny_type + $this->glue_notch * $this->glueing;
        } else {
            $roh = $this->wall_area * $et_type + $this->floor_area * $et_type + $this->glue_notch * $this->glueing;
        }

        // ${"ny".$this->floor_thickness * 1000}

        switch ($this->surface_options['color_option']) {
            case 'Standardfarbe':
            case 'RAL-Farbe':
            case 'NCS-Farbe':
                $color_costs = $roh + $this->total_area * $this->ikosit;
                break;
            case 'Buntsteinputz':
                $color_costs = $roh + ($this->total_area - $this->wall_area) * $this->inertol + $this->wall_area * $this->marmolit;
                break;
            case 'Reibputz':
                $color_costs = $roh + ($this->total_area - $this->wall_area) * $this->inertol + $this->wall_area * $this->silikat;
                break;
            default:
                $trough_error = 'Es wurde keine Farboption angegeben';
        }

        $this->trough_price += $color_costs;
    }

    /**
     * Compute the water options.
     *
     * @return void
     */
    public function computeWater()
    {
        switch ($this->water_option) {
            case 'Völlige Entleerung':
                $this->trough_price += 0;
                break;
            case 'Wasserspeicher':
                $this->trough_price += 0;
                break;
            case 'Hydrodicht':
                $this->trough_price += $this->hydro_density;
                break;
            default: $trough_error = 'Es wurde keine Wasserablaufoption angegeben';
        }
    }

    /**
     * Compute the optional profile, which is placed on the top sides of the trough. The profile can be chosen individually.
     *
     * @return void
     */
    public function computeProfile()
    {
        $country_meter = 0;

        if ($this->profile_options['profile_front'] == 1) {
            $country_meter += ($this->measurements['length'] + 0.1) * $this->offcut;
        }
        if ($this->profile_options['profile_left'] == 1) {
            $country_meter += ($this->measurements['width'] + 0.1) * $this->offcut;
        }
        if ($this->profile_options['profile_back'] == 1) {
            $country_meter += ($this->measurements['length'] + 0.1) * $this->offcut;
        }
        if ($this->profile_options['profile_right'] == 1) {
            $country_meter += ($this->measurements['width'] + 0.1) * $this->offcut;
        }

        $country_house_extracharge = $this->country_house_price * $country_meter;
        $this->trough_price += $country_house_extracharge;
    }

    /**
     * Compute optional accessoires.
     *
     * @return void
     */
    public function computeAccessoires()
    {
        if ($this->additional_options['vlies'] == true) {
            $vliesMenge = (($this->measurements['length'] * 1000 + 100) * ($this->measurements['width'] * 1000 + 100) * 1.25) / 1000000;
            $vlies_costs = ($vliesMenge * $this->vlies_price) + ((2 * ($this->measurements['length'] * 1000 + 100 + $this->measurements['width'] * 1000 + 100) * 2) / 1000);
            $this->trough_price += $vlies_costs;
        }
        if ($this->additional_options['leca'] == true) {
            $lecaMenge = (($this->measurements['length'] * $this->measurements['width']) *  $this->leca_height / 100) * 1000;
            $leca_costs = ($lecaMenge * 0.2) + ($this->leca_filling * $lecaMenge);
            $this->trough_price += $leca_costs;
        }
        if ($this->additional_options['styropor'] == true) {
            $styropor_area = (($this->measurements['length'] * $this->measurements['height'] * 2) + ($this->measurements['width'] * $this->measurements['height'] * 2) + ($this->measurements['length'] * $this->measurements['width']));
            $styropor_costs = $styropor_area * $this->styropor_price * 1.2 + ($styropor_area * 5 * ($this->working_hour / 60));
            $this->trough_price += $styropor_costs;
        }

        $this->trough_price = round($this->trough_price, 2);
    }

    /**
     * Compute weights and volume of the trough.
     *
     * @return void
     */
    public function computeWeight()
    {
        $middle_piece_weight = ($this->middle_piece == true) ? ((($this->measurements['height'] - $this->floor_thickness - $this->floor_clearance - 0.15) * ($this->measurements['width'] - ($this->wall_options['wall_thickness'] * 2)) * $this->wall_options['wall_thickness'] * 1250)) : 0;

        $this->weight_empty = $this->wall_area * $this->wall_options['wall_thickness'] * 1250 + $this->floor_area * $this->floor_thickness * 1250 + $middle_piece_weight;
        $this->weight_empty = round($this->weight_empty, 2);
        $this->volume = $this->inner_height * $this->inner_width * $this->inner_length * 1000;
        $this->volume = (int)$this->volume;
        $this->weight_earth = $this->weight_empty + $this->volume * 1.2;
        $this->weight_earth = round($this->weight_earth, 2);
    }

    /**
     * Compute the stability of the trough for further calculations.
     *
     * @return void
     */
    public function computeStatics($middlePiece)
    {
        $statiklength = ($middlePiece == false) ? $this->measurements['length'] : $this->measurements['length'] / 2;
        $statikinner_length = ($middlePiece == false) ? $this->inner_length : $this->inner_length / 2;
        $this->middle_piece = $middlePiece;

        $E = 3000000;                                                         #KN/m2
        $GammaFeucht = 18;                                                    #KN/m3  spezifisches Gewicht Erde feucht
        $Rho = 20;
        $Rho = 2 * 3.141593 * $Rho / 360;                                     #Grad   Reibungswinkel Umwandlung Bogenmaß
        $lambda = (1 - sin($Rho)) / (1 + sin($Rho));
        $DeltaA = $lambda * $GammaFeucht * $this->inner_height;                 #KN/m2  erdfeuchter Boden
        $Kraft1 = $DeltaA * $this->inner_height * $statikinner_length * 0.5;     #KN  Gewicht Erde feucht

        $gammaSatt = 8;                                                       #KN/m3  spez.Gewicht ges„ttigter Erde
        $DeltaB = $lambda * $gammaSatt * $this->inner_height;                         #KN/m2  Boden
        $DeltaW = 10 * $this->inner_height;                                           #KN/m2  Wasser
        $DeltaGes = $DeltaB + $DeltaW;
        $Kraft2 = $DeltaGes * $this->inner_height * $statikinner_length * 0.5;
        $SigmaZ = $Kraft2 / (($this->floor_thickness + 2 * $this->notch_depth) * $statikinner_length);#KN/m2  Zugspannung Fuge
        $Faktor = $Kraft2 / $Kraft1;

        #Längswand mit erdfeuchtem Boden belastet
        $Lx1 = $statikinner_length;
        $Ly1 = $this->inner_height;
        $Epsilon1 = $Ly1 / $Lx1;

        /***************************************DREISEITIG EINGESPANNTE PLATTE***************************************/
        switch ($Epsilon1) {
            case ($Epsilon1<= 0.249): $Nmin = 6; $P1 = 0.3; break;
            case ($Epsilon1<= 0.3): $N1 = 6.92; $N2 = 7.56; $P1 = 0.26;
                                    $Nmin = $N2 - ($N2 - $N1) * 10 * (0.3 - $Epsilon1) / 2; break;
            case ($Epsilon1<= 0.4): $N1 = 7.56; $N2 = 9.35; $P1 = 0.19;
                                    $Nmin = $N2 - ($N2 - $N1) * 10 * (0.4 - $Epsilon1); break;
            case ($Epsilon1<= 0.5): $N1 = 9.35; $N2 = 11.68; $P1 = 0.116;
                                    $Nmin = $N2 - ($N2 - $N1) * 10 * (0.5 - $Epsilon1); break;
            case ($Epsilon1<= 0.6): $N1 = 11.68; $N2 = 14.43; $P1 = 0.0675;
                                    $Nmin = $N2 - ($N2 - $N1) * 10 * (0.6 - $Epsilon1); break;
            case ($Epsilon1<= 0.7): $N1 = 14.43; $N2 = 17.49; $P1 = 0.039;
                                    $Nmin = $N2 - ($N2 - $N1) * 10 * (0.7 - $Epsilon1); break;
            case ($Epsilon1<= 0.8): $N1 = 17.49; $N2 = 20.86; $P1 = 0.024;
                                    $Nmin = $N2 - ($N2 - $N1) * 10 * (0.8 - $Epsilon1); break;
            case ($Epsilon1<= 0.9): $N1 = 20.86; $N2 = 24.25; $P1 = 0.016;
                                    $Nmin = $N2 - ($N2 - $N1) * 10 * (0.9 - $Epsilon1); break;
            case ($Epsilon1<= 1): $N1 = 24.25; $N2 = 28.47; $P1 = 0.0115;
                                  $Nmin = $N2 - ($N2 - $N1) * 10 * (1 - $Epsilon1); break;
            case ($Epsilon1<= 1.1): $N1 = 28.47; $N2 = 32.75; $P1 = 0.0085;
                                    $Nmin = $N2 - ($N2 - $N1) * 10 * (1.1 - $Epsilon1); break;
            case ($Epsilon1<= 1.2): $N1 = 32.75; $N2 = 37.37; $P1 = 0.0065;
                                    $Nmin = $N2 - ($N2 - $N1) * 10 * (1.2 - $Epsilon1); break;
            case ($Epsilon1<= 1.3): $N1 = 37.37; $N2 = 42.37; $P1 = 0.005;
                                    $Nmin = $N2 - ($N2 - $N1) * 10 * (1.3 - $Epsilon1); break;
            case ($Epsilon1<= 1.4): $N1 = 42.37; $N2 = 47.68; $P1 = 0.0035;
                                    $Nmin = $N2 - ($N2 - $N1) * 10 * (1.4 - $Epsilon1); break;
            case ($Epsilon1<= 1.5): $N1 = 47.68; $N2 = 53.28; $P1 = 0.003;
                                    $Nmin = $N2 - ($N2 - $N1) * 10 * (1.5 - $Epsilon1); break;
            default: $Nmin = 55; $P1 = 0.003; break;
        }

        $Mwandhor = ($DeltaA * pow($Ly1, 2)) / $Nmin;                                        #KNm maximales Drehmoment
        $Wwandhor = (1 * pow($this->wall_options['wall_thickness'], 2)) / 6;                                          #m3 Widerstandsmoment
        $SigmaA = $Mwandhor / $Wwandhor;                                                     #KN/m2 Biegespannung
        $this->bending_wall = (($DeltaA * pow($Ly1, 4)) / ($E * pow($this->wall_options['wall_thickness'], 3))) * $P1;            #m1 Durchbiegung

        #Längswand mit gesättigtem Erd-Boden
        $SigmaB = $SigmaA * $Faktor;
        $FwandB = $this->bending_wall * $Faktor;

        #Längswand vertikale Belastung
        $qvert = $GammaFeucht * ($this->inner_height + $this->floor_thickness);                #weight_emptyeichlast Trog je m2 ,Wandstärke als Eigengewichtersatz mitgerechnet
        $qwand = $qvert * $this->measurements['width'] * 0.5;                                      #weight_emptyeichlast für eine Längswand

        $A = 0.165;
        $B = 1 - 2 * 0.165;
        $Ma = -($qwand * pow(($statiklength * $A), 2)) / 2;                         #Stützmoment
        $Mb = ($qwand * pow(($statiklength * $B), 2)) / 8;                          #Feldmoment
        $Mab = $Ma + $Mb;                                                     #KNm
        $Q1 = $qwand * $statiklength * 0.5;                                         #KN  Querkraft (Auflagerkraft)
        $Wwandvert = ($this->wall_options['wall_thickness'] * pow($this->measurements['height'], 2)) / 6;                     #m3 Widerstandsmoment
        $SigmaC = $Mab / $Wwandvert + $SigmaB;

        #Bodenplatte  Wandstärken mitrechnen, ersetzt das Eigengewicht
        $qvert = $GammaFeucht * ($this->inner_height + $this->floor_thickness);
        $Lx2 = $statiklength * $B;
        $Ly2 = $this->inner_width;

        if ($Lx2 > $Ly2) {
            $Ly2 = $Lx2;
            $Lx2 = $this->inner_width;
        }

        $Epsilon2 = $Ly2 / $Lx2;

        /***************************************VIERSEITIG EINGESPANNTE PLATTE***************************************/
        switch ($Epsilon2) {
            case ($Epsilon2 <= 1): $Nmin = 19.4; $P1 = 0.015; break;
            case ($Epsilon2 <= 1.05): $N1 = 19.4; $N2 = 18.2; $P1 = 0.01595;
                                      $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1) * 20; break;
            case ($Epsilon2 <= 1.1): $N1 = 18.2; $N2 = 17.1; $P1 = 0.0174;
                                     $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.05) * 20; break;
            case ($Epsilon2 <= 1.15): $N1 = 17.1; $N2 = 16.3; $P1 = 0.0188;
                                      $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.1) * 20; break;
            case ($Epsilon2 <= 1.2): $N1 = 16.3; $N2 = 15.5; $P1 = 0.0201;
                                     $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.15) * 20; break;
            case ($Epsilon2 <= 1.25): $N1 = 15.5; $N2 = 14.9; $P1 = 0.0213;
                                      $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.2) * 20; break;
            case ($Epsilon2 <= 1.3): $N1 = 14.9; $N2 = 14.5; $P1 = 0.0224;
                                     $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.25) * 20; break;
            case ($Epsilon2 <= 1.35): $N1 = 14.5; $N2 = 14; $P1 = 0.0235;
                                      $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.3) * 20; break;
            case ($Epsilon2 <= 1.4): $N1 = 14; $N2 = 13.7; $P1 = 0.0244;
                                     $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.35) * 20; break;
            case ($Epsilon2 <= 1.45): $N1 = 13.7; $N2 = 13.4; $P1 = 0.02525;
                                      $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.4) * 20; break;
            case ($Epsilon2 <= 1.5): $N1 = 13.4; $N2 = 13.2; $P1 = 0.02605;
                                     $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.45) * 20; break;
            case ($Epsilon2 <= 1.55): $N1 = 13.2; $N2 = 12.8; $P1 = 0.02675;
                                      $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.5) * 20; break;
            case ($Epsilon2 <= 1.6): $N1 = 12.8; $N2 = 12.7; $P1 = 0.0274;
                                     $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.55) * 20; break;
            case ($Epsilon2 <= 1.65): $N1 = 12.7; $N2 = 12.5; $P1 = 0.02795;
                                      $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.6) * 20; break;
            case ($Epsilon2 <= 1.7): $N1 = 12.5; $N2 = 12.4; $P1 = 0.02845;
                                     $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.65) * 20; break;
            case ($Epsilon2 <= 1.75): $N1 = 12.4; $N2 = 12.3; $P1 = 0.0289;
                                      $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.7) * 20; break;
            case ($Epsilon2 <= 1.8): $N1 = 12.3; $N2 = 12.2; $P1 = 0.02925;
                                     $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.75) * 20; break;
            case ($Epsilon2 <= 1.85): $N1 = 12.2; $N2 = 12.1; $P1 = 0.02955;
                                     $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.8) * 20; break;
            case ($Epsilon2 <= 1.9): $N1 = 12.1; $N2 = 12; $P1 = 0.02985;
                                    $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.85) * 20; break;
            case ($Epsilon2 <= 1.95): $N1 = 12; $N2 = 12; $P1 = 0.0301;
                                      $Nmin = $N1 - ($N1 - $N2) * ($Epsilon2 - 1.9) * 20; break;
            case ($Epsilon2 <= 2): $Nmin = 12; $P1 = 0.0303; break;
            default: $trough_error = "Boden > 2"; break;
        }

        if ($Epsilon2 > 2) {
            $Mbod = ($qvert * pow($Lx2, 2)) / 12;
            $Wbod = (1 * pow($this->floor_thickness, 2)) / 6;
            $Q2 = $DeltaGes * $Lx2 * 0.5;
            $SigmaD = $Q2 / (1 * $this->floor_thickness) + $Mbod / $Wbod;
            $I = (1 * pow($this->floor_thickness, 3)) / 12;
            $this->bending_floor = ($qvert * pow($Lx2, 4)) / (384 * $E * $I);
        } else {
            $Mbod = ($qvert * pow($Lx2, 2)) / $Nmin;
            $Wbod = (1 * pow($this->floor_thickness, 2)) / 6;
            $Q2 = $DeltaGes * $Lx2 * 0.5;
            $SigmaD = $Q2 / (1 * $this->floor_thickness) + $Mbod / $Wbod;
            $this->bending_floor = (($qvert * pow($Lx2, 4)) / ($E * pow($this->floor_thickness, 3))) * $P1;
        }
    }

    /**
     * Compute the needed work time for the calculation.
     *
     * @return void
     */
    public function computeTime()
    {
        $roh = $this->glue_notch * 31;                                                                    #31 Minuten je m2 glue_notch
        $zf = $roh + $this->total_area * 21;
        $zf = (int)$zf;                                                                            #21 Minuten je m2 Farbfläche
        $zr = $roh + ($this->total_area - $this->wall_area) * 21 + $this->wall_area * 42;
        $zr = (int)$zr;                                                                            #21 Min Inertol  42 Min Putz je m2
        $zb = $roh + ($this->total_area - $this->wall_area) * 21 + $this->wall_area * 42;
        $zb = (int)$zb;                                                                            #21 Min Inertol  42 Min Marmolit je m2

        switch ($this->surface_options['color_option']) {
            case 2:
                $zeit = $zf;
                break;
            case 3:
                $zeit = $zr;
                break;
            case 4:
                $zeit = $zb;
                break;
        }
    }

    /**
     * Call this method to update the prices and stats of the trough.
     *
     * @return void
     */
    public function finishTrough()
    {
        $_SESSION['trough-price'] = $this->trough_price;
        $_SESSION['surface'] = $this->surface;

        return $this->bending_wall;
    }

    /**
     * Reset all values of the trough in need of recalculation.
     *
     * @return void
     */
    public function clearTrough()
    {
        $this->surface = 0;
        $this->weight_empty = 0;
        $this->weight_earth = 0;
        $this->surface = 0;
        $this->trough_price = 0;
        $this->volume = 0;
    }
}
