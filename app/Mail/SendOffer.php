<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Offer;

class SendOffer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The offer instance.
     *
     * @var Offer
     */
    public $offer;
    public $contact;

    /**
     * Desription of the mail content.
     *
     * @var String
     */
    public $header;
    public $description;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Offer $offer, $contact)
    {
        $this->offer = $offer;
        $this->contact = $contact;
        $this->header = 'Vielen Dank für Ihre Anfrage';
        $this->description = 'Im Anhang finden Sie Ihr persönliches Produktangebot.
                              Wir werden uns umgehend bei Ihnen melden,
                              um Sie über den Status Ihrer Anfrage zu informieren.';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Ihr unverbindliches Offert bei TONDO®')
                    ->view('emails.offer');
        #             ->attach('/path/to/file', [
                        #'as' => 'name.pdf',
                        #'mime' => 'application/pdf',
                    #]);
    }
}
