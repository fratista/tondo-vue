<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Bei Tondo erhalten Sie Pflanzentröge nach Maß. Jeder Pflanzentrog kann individuell konfiguriert werden. Bestellen Sie Ihr Pflanzengefäß in unserem Onlineshop" />
    <meta name="author" content="" />
    <meta name="keywords" content="Pflanzengefäße, Pflanztrog, Pflanzgefäß, Pflanzkübel, Pflanzenkübel, Pflanzentröge nach Mass,
    Blumenkübel, Blumentopf, Blumentöpfe, Blumentrog, Betongefaesse, Dachgarten, Bänke, Terrassenmöbel, Corten Stahl,Gartengestaltung,
    Gartenarchitekt, Gartenkunst, Hydrokultur, Landschaftsarchitekten, Faserzement, Terrakotta, Tondo, Topf, Töpfe, Trog, Tröge, Kübel" />
    <meta name="robots" content="index, follow, all">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- jQuery and Popper.js for Bootstrap4 -->
    <script src="lib/jquery/jquery-3.3.1.min.js"></script>
    <script src="lib/popper.js/umd/popper.min.js"></script>

    <!-- Bootstrap4 -->
    <link href="lib/bootstrap4/css/bootstrap.css" type="text/css" rel="stylesheet" />
    <script src="lib/bootstrap4/js/bootstrap.js"></script>

    <!-- Vue.js + Axios + Three.js -->
    <script src="lib/vue.js/vue.js"></script>
    <script src="lib/axios/dist/axios.min.js"></script>
    <script src="lib/three.js/build/three.min.js"></script>

    <!-- Vue components -->
    <script src="js/components/configurator.vue"></script>
    <script src="js/components/rendering.vue"></script>

    <!-- General app script -->
    <script src="js/app.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/tondo.css') }}" type="text/css" rel="stylesheet" />
    <link href="{{ asset('css/ionicons/css/ionicons.min.css') }}" type="text/css" rel="stylesheet" />

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
</head>

<body>
    <div id="app">
        @yield('content')
        @include('partials.modal')
    </div>
</body>

</html>
