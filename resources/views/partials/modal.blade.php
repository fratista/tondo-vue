<div class="modal" id="request-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Anfrage</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Bitte geben Sie Ihre Kontaktdaten an, damit wir eine Kosteninformation für Sie zusammenstellen können.</p>
        <form id="request-form">
          <div class="form-group">
            <input type="email" class="form-control" name="email" placeholder="E-Mail Adresse*" required>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="firstname" placeholder="Vorname*" required>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="lastname" placeholder="Nachname*" required>
            <small id="emailHelp" class="form-text text-muted mt-4">Wir geben Ihre Daten nicht an Dritte weiter und verwenden diese lediglich zur Kontaktaufnahme.</small>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-tondo request-offer" data-toggle="modal" data-target="#confirmation-modal">Anfragen</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="confirmation-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Bestätigung</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Vielen Dank für Ihre Anfrage. Sie erhalten in Kürze ein Mail mit den relevanten Daten Ihrer Anfrage. Wir werden uns nach Durchsicht bei Ihnen melden.</p>
      </div>
    </div>
  </div>
</div>
